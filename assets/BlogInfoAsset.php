<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

class BlogInfoAsset extends AssetBundle
{

  public $basePath = '@webroot';
  public $baseUrl = '@web';
  public $css = [
  ];
  public $js = [
    'js/blog-info.js'
  ];
  public $depends = [
    'yii\web\JqueryAsset',
    'yii\bootstrap\BootstrapAsset',
  ];

}