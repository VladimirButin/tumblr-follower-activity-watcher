This is application is small clone of Stalkr.
It uses Yii2 Framework and various assets such as JQuery and Bootstrap.
This application uses Tumblr API to check your blogs followers and your followees. 
All data is logged, so you can watch history of changes.
By clicking blogs avatars in followers and followees you can watch blogs info.
By clicking your blog avatar at index page you may see your blog followers.
It uses authorization via Tumblr OAuth.
It also can automatically update history via command "php yii grabber/update". It updates ALL users, so use carefully.

It requires PHP 5.6 or higher, MySQL and Composer. For installation, launch command "composer install".
You also need to configure "db.php" and "oauth.php" at config dir. Examples are provided.
After you configure db, don't forget to run "php yii migrate/up --all" for creating tables.
Pagination options are stored in params.