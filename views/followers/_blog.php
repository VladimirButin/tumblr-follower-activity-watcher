<?php

use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var $model \app\models\Followee
 */
?>
<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 followee">
  <div class="avatar-block row">
      <?php
      $url = "https://api.tumblr.com/v2/blog/{$model->name}.tumblr.com/avatar/128";
      $img = Html::img($url);
      echo Html::a($img, ['blog-info/index', 'blog' => $model->name], [
        'data-blog' => $model->name,
        'class' => 'blog-info'
      ]);
      ?>
  </div>
  <div class="link-block row">
      <?php
      echo Html::a($model->name, $model->url);
      ?>
  </div>
  <?php if (!$model->convertedInfo['following']) : ?>
    <div class="link-block row">
      <span class="label label-danger">
        Not followed back
      </span>
    </div>    <?php else: ?>
    <div class="link-block row">
      <span class="label label-primary">
        Followed back
      </span>
    </div>
  <?php endif; ?>
</div>


