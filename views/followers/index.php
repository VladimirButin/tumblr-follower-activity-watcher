<?php

use yii\widgets\ListView;
use yii\helpers\Html;
use yii\widgets\Pjax;

\app\assets\BlogInfoAsset::register($this);

/* @var $this yii\web\View */

$this->title = 'Followers - Tumblr Follower Activity Watcher';
?>
<h2><?= Html::encode($blogData->blogname) ?> followers</h2>

<?php Pjax::begin(['id' => 'FollowersPjax']); ?>
<?php
echo ListView::widget([
  'dataProvider' => $dataProvider,
  'itemView' => '_blog',
  'layout' => "{summary}\n<div class=\"row\">{items}</div>{pager}"
]);
?>
<?php Pjax::end(); ?>