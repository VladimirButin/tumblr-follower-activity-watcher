<?php

use yii\helpers\Html;

/**
 * @var $model \app\models\Followee
 */
?>
<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 followee">
  <div class="avatar-block row">
      <?php
      $url = "https://api.tumblr.com/v2/blog/{$model->name}.tumblr.com/avatar/128";
      $img = Html::img($url);
      echo Html::a($img, ['blog-info/index', 'blog' => $model->name], [
        'data-blog' => $model->name,
        'class' => 'blog-info'
      ]);
      ?>
  </div>
  <div class="link-block row">
    <?php
    echo Html::a($model->name, $model->url);
    ?>
  </div>
</div>


