<?php

use yii\widgets\ListView;
use yii\widgets\Pjax;

\app\assets\BlogInfoAsset::register($this);

/* @var $this yii\web\View */

$this->title = 'Followees - Tumblr Follower Activity Watcher';
?>
<h2>Your followees</h2>

<?php Pjax::begin(['id' => 'FolloweesPjax']); ?>
<?php
echo ListView::widget([
  'dataProvider' => $dataProvider,
  'itemView' => '_blog',
  'layout' => "{summary}\n<div class=\"row\">{items}</div>{pager}"
]);
?>
<?php Pjax::end(); ?>