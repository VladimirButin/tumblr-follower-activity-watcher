<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\daterange\DateRangePicker;

\app\assets\BlogInfoAsset::register($this);

/* @var $this yii\web\View */
/* @var $searchModel app\models\LogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'History - Tumblr Follower Activity Watcher';
?>
<div class="log-index">

  <h1>History</h1>
  <?php Pjax::begin(['id' => 'LogPjax']); ?>
  <?=
  GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
      [
        'attribute' => 'userBlog',
        'format' => 'raw',
        'value' => function ($model) {
          if ($model->blog) {
            $imgUrl = "https://api.tumblr.com/v2/blog/{$model->blog->blogname}.tumblr.com/avatar/24";
            $img = Html::img($imgUrl);
            return Html::a($img . ' ' . $model->blog->blogname, Url::to(['followers/index', 'blogId' => $model->userBlog]), [
              'data-pjax' => 0,
            ]);
          }
          return '';
        },
        'filter' => $blogs,
      ],
      [
        'attribute' => 'blogName',
        'format' => 'raw',
        'value' => function($model) {
          $imgUrl = "https://api.tumblr.com/v2/blog/{$model->blogName}.tumblr.com/avatar/24";
          $img = Html::img($imgUrl);
          return Html::a($img . ' ' . Html::encode($model->blogName), ['blog-info/index', 'blog' => $model->blogName], [
            'data-blog' => $model->blogName,
            'class' => 'blog-info'
          ]);
        },
      ],
      'blogUrl:url',
      [
        'attribute' => 'action',
        'value' => function ($model) use ($actionTitles) {
          return $actionTitles[$model->action];
        },
        'filter' => $actionTitles,
      ],
      [
        'attribute' => 'date',
        'format' => 'datetime',
        'filter' => DateRangePicker::widget([
          'model' => $searchModel,
          'attribute' => 'date',
          'convertFormat' => true,
          'useWithAddon' => false,
          'pluginOptions' => [
            'locale' => [
              'format' => 'd M Y',
              'separator' => Yii::$app->params['dateSeparator'],
            ],
            'opens' => 'left'
          ]
        ])
      ]
    ],
  ]);
  ?>
  <?php Pjax::end(); ?>
</div>
