<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */

$this->title = 'Tumblr Follower Activity Watcher';
?>

<?php if (Yii::$app->user->isGuest): ?>
  <div class="jumbotron" >
    <div class="row">
      <div class="col-sm-10 col-sm-offset-2">
        <h3>For using this application you should login via Tumblr.</h3>
      </div>    
    </div>
  </div>
<?php else: ?>
  <?php
  $userInfo = yii\helpers\Json::decode(Yii::$app->user->identity->info);
  ?>
  <div class="jumbotron" >
    <div class="row">
      <div class="col-xs-12">
        <strong>Name:</strong> <?= Html::encode($userInfo['name']); ?>
      </div>    
    </div>
    <div class="row">
      <div class="col-xs-12">
        <strong>Last login:</strong> <?= Yii::$app->formatter->asDatetime(Yii::$app->user->identity->lastLogin); ?>
      </div>    
    </div>
    <div class="row">
      <div class="col-xs-12">                
        <strong>Likes:</strong> <?= Html::encode($userInfo['likes']); ?>
      </div>    
    </div>
    <div class="row">
      <div class="col-xs-12">
        <strong>Following:</strong> <?= Html::encode($userInfo['following']); ?>
      </div>    
    </div>
  </div>
  <div class="row">
    <div class="col-xs-12">
      <h2>
        Related blogs
      </h2>
    </div>
  </div>
  <?php Pjax::begin(['id' => 'BlogsPjax']); ?>
  <?php
  echo GridView::widget([
    'dataProvider' => $dataProvider,
    'sorter' => [],
    'summary' => "",
    'columns' => [
      [
        'label' => 'Avatar',
        'format' => 'raw',
        'value' => function($data) {
          $url = "https://api.tumblr.com/v2/blog/{$data->convertedInfo['uuid']}/avatar";
          $img = Html::img($url);
          return Html::a($img, Url::to(['followers/index', 'blogId' => $data['id']]), [
            'data-pjax' => 0,
          ]);
        }
      ],
      [
        'label' => 'Blog',
        'format' => 'raw',
        'value' => function($data) {
          return Html::a(Html::encode($data->convertedInfo['name']), $data->convertedInfo['url']);
        }
      ],
      [
        'label' => 'Description',
        'format' => 'raw',
        'value' => function($data) {
          return $this->render('_blog', ['info' => $data->convertedInfo]);
        }
      ]
  ]]);
  ?>
  <?php Pjax::end(); ?>
<?php endif; ?>




