<?php

use yii\helpers\Html;
?>
<div class="container-fluid">   
  <div class="row">
    <strong>Title:</strong> <?= Html::encode($info['title']); ?>
  </div>
  <div class="row">
    <strong>Updated:</strong> <?= Yii::$app->formatter->asDatetime($info['updated']) ?>
  </div>
  <div class="row">
    <strong>Posts:</strong> <?= Html::encode($info['posts']); ?>
  </div>
  <div class="row">
    <strong>Followers:</strong> <?= Html::encode($info['followers']); ?>
  </div>
  <div class="row">
    <strong>NSFW:</strong> <?= $info['is_nsfw'] ? "Yes" : "No" ?>
  </div>
  <div class="row">
    <strong>Description:</strong> <?= Html::encode($info['description']); ?>
  </div>
</div>


