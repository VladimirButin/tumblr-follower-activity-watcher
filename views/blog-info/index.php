<?php

use yii\widgets\DetailView;
use yii\helpers\Html;

$this->title = 'Blog info - Tumblr Follower Activity Watcher';
?>

<?=
DetailView::widget([
  'model' => $blogInfo,
  'attributes' => [
    [
      'label' => 'Avatar',
      'value' => Html::a(Html::img("https://api.tumblr.com/v2/blog/{$blogInfo->name}.tumblr.com/avatar/128"), $blogInfo->url),
      'format' => 'raw',
    ],
    [ 'attribute' => 'name',
      'value' => Html::a($blogInfo->name, $blogInfo->url),
      'format' => 'raw',
    ],
    'title',
    'updated:datetime',
    'posts',
    [
      'attribute' => 'likes',
      'value' => $blogInfo->share_likes ? Html::a($blogInfo->likes, 'http://tumblr.com/liked/by/' . $blogInfo->name . '/') : 'Not available',
      'format' => 'raw',
    ],
    [
      'attribute' => 'is_nsfw',
      'value' => $blogInfo->is_nsfw ? 'Yes' : 'No',
    ],
    'description',
  ]
]);
?>


