<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\bootstrap\Alert;
use yii\bootstrap\Modal;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
  <head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
  </head>
  <body>
      <?php $this->beginBody() ?>

    <div class="wrap">
        <?php
        NavBar::begin([
          'brandLabel' => 'Tumblr Follower Activity Watcher',
          'brandUrl' => Yii::$app->homeUrl,
          'options' => [
            'class' => 'navbar-default',
          ],
        ]);
        echo Nav::widget([
          'options' => ['class' => 'navbar-nav navbar-right'],
          'items' => Yii::$app->user->isGuest ?
          [
            [
              'label' => 'Login',
              'url' => ['/site/login'],
            ],
          ] :
          [
            [
              'label' => 'Update data',
              'url' => ['/site/update'],
            ],
            [
              'label' => 'Blogs',
              'url' => ['/site/index'],
            ],
            [
              'label' => 'Followees',
              'url' => ['/followees/index'],
            ],
            [
              'label' => 'History',
              'url' => ['/log/index'],
            ],
            [
              'label' => 'Logout (' . Yii::$app->user->identity->username . ')',
              'url' => ['/site/logout'],
              'linkOptions' => ['data-method' => 'post'],
            ],
          ],
        ]);
        NavBar::end();
        ?>

      <?php
      Modal::begin([
        'header' => '<h2>Blog info</h2>',
        'id' => 'blogInfoModal',
      ]);
      ?>
      <div id="modalContent">

      </div>
      <?php Modal::end(); ?>

      <div class="container">
        <?php
        if (Yii::$app->session->hasFlash('alert')) {
          echo Alert::widget([
            'options' => [
              'class' => 'alert-info',
            ],
            'body' => Yii::$app->session->getFlash('alert')
          ]);
        }
        ?>
        <?= $content ?>
      </div>
    </div>

    <?php $this->endBody() ?>
  </body>
</html>
<?php $this->endPage() ?>
