<?php

use yii\db\Schema;
use yii\db\Migration;

class m151230_113026_indexes extends Migration
{
  public function safeUp()
  {
    $this->createIndex('for_index', 'blog', ['userId', 'active']);

    $this->createIndex('active_followees', 'followee', ['userId', 'active', 'name']);
    $this->createIndex('for_checking', 'followee', ['userId', 'url']);

    $this->createIndex('active_followers', 'follower', ['userId', 'active', 'blogId', 'name']);
    $this->createIndex('for_checking', 'follower', ['userId', 'url', 'blogId']);
    
    $this->createIndex('for_search', 'log', ['userBlog', 'date', 'id']);
  }

  public function safeDown()
  {
    $this->dropIndex('for_index', 'blog');

    $this->dropIndex('active_followees', 'followee');
    $this->dropIndex('for_checking', 'followee');

    $this->dropIndex('active_followers', 'follower');
    $this->dropIndex('for_checking', 'follower');
      
    $this->dropIndex('for_search', 'log');
  }
}
