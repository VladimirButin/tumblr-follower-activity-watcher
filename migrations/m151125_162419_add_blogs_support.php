<?php

use yii\db\Schema;
use yii\db\Migration;

class m151125_162419_add_blogs_support extends Migration
{

    public function up()
    {
        $this->addColumn('user', 'info', $this->text());
        $this->createTable('blog', [
            'id' => $this->primaryKey(),
            'userId' => $this->integer()->notNull(),
            'blogname' => $this->string('100')->notNull(),
            'active' => $this->boolean()->defaultValue('1'),
            'info' => $this->text(),
        ]);
    }

    public function down()
    {
        $this->dropTable('blog');
        $this->dropColumn('user', 'info');
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
