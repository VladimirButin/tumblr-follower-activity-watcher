<?php

use yii\db\Schema;
use yii\db\Migration;

class m151126_123642_add_followees_support extends Migration
{

    public function up()
    {
        $this->createTable('followee', [
            'id' => $this->primaryKey(),
            'userId' => $this->integer()->notNull(),
            'name' => $this->string(100)->notNull(),
            'url' => $this->string(100)->notNull(),
            'active' => $this->smallInteger()->defaultValue("1"),
            'info' => $this->text(),
        ]);
    }

    public function down()
    {
        $this->dropTable('followee');
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
