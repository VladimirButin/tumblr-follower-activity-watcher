<?php

use yii\db\Schema;
use yii\db\Migration;

class m151124_094826_create_user_table extends Migration
{

    public function up()
    {
        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'username' => $this->string('100')->notNull()->unique(),
            'oauthToken' => $this->string('100')->notNull(),
            'oauthTokenSecret' => $this->string('100')->notNull(),
            'lastLogin' => $this->bigInteger(),
            'authKey' => $this->string('100')->notNull(),
        ]);
    }

    public function down()
    {
        $this->dropTable('user');
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
