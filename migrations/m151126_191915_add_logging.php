<?php

use yii\db\Schema;
use yii\db\Migration;

class m151126_191915_add_logging extends Migration
{

    public function up()
    {
        $this->createTable('log', [
            'id' => $this->primaryKey(),
            'userId' => $this->integer()->notNull(),
            'userBlog' => $this->integer(),
            'blogName' => $this->string(100)->notNull(),
            'blogUrl' => $this->string(100)->notNull(),
            'action' => $this->smallInteger(),
            'date' => $this->bigInteger(),
        ]);
    }

    public function down()
    {
        $this->dropTable('log');
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
