<?php

use yii\db\Schema;
use yii\db\Migration;

class m151227_152919_add_access_token_blob extends Migration
{

  public function up()
  {
    $this->addColumn('user', 'access_token_blob', $this->binary());
    $this->dropColumn('user', 'oauthToken');
    $this->dropColumn('user', 'oauthTokenSecret');
  }

  public function down()
  {
    $this->dropColumn('user', 'access_token_blob');
    $this->addColumn('user', 'oauthToken', $this->string('100')->notNull());
    $this->addColumn('user', 'oauthTokenSecret', $this->string('100')->notNull());
  }
}