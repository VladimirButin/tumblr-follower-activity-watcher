<?php

namespace app\commands;

use Yii;
use yii\console\Controller;
use yii\helpers\Console;
use yii\helpers\VarDumper;
use yii\base\Exception;
use app\models\User;
use app\models\update\DataUpdate;

class GrabberController extends Controller
{

  public function actionUpdate()
  {
    try {
      $this->stdout("Work started\n\n");
      $dataUpdate = new DataUpdate();
      $oauthClient = Yii::$app->oauth;

      foreach (User::find()->each() as $user) {
        $this->stdout("Updating data for user '{$user->username}'...");

        $oauthClient->setAccessToken($user->getAccessTokenBlob());
        $dataUpdate->update($user->id);

        $this->stdout("Done\n");
      }

      $this->stdout("\nWork finished\n");
    } catch (Exception $ex) {
      $this->stdout("An error occurred", Console::BG_RED);
      $this->stdout("\n");
      $this->stdout(VarDumper::dumpAsString($ex));
    }
  }
}