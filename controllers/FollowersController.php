<?php

namespace app\controllers;

use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use Yii;
use app\models\Follower;
use app\models\Blog;

class FollowersController extends \yii\web\Controller
{

  public function behaviors()
  {
    return [
      'access' => [
        'class' => AccessControl::className(),
        'only' => ['index'],
        'rules' => [
          [
            'actions' => ['index'],
            'allow' => true,
            'roles' => ['@'],
          ],
        ],
      ],
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'index' => ['get'],
        ],
      ],
    ];
  }

  public function actions()
  {
    return [
      'error' => [
        'class' => 'yii\web\ErrorAction',
      ],
    ];
  }

  public function actionIndex($blogId)
  {
    if (empty($blogId)) {
      return $this->goHome();
    }
    $blogData = Blog::getByUserAndId(Yii::$app->user->id, $blogId);
    if (empty($blogData)) {
      return $this->goHome();
    }

    $query = Follower::getActiveFollowersQuery(Yii::$app->user->id, $blogId);
    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'pagination' => [
        'pageSize' => Yii::$app->params['pagination']['pageSize'],
      ],
    ]);

    return $this->render('index', [
      'dataProvider' => $dataProvider,
      'blogData' => $blogData,
    ]);
  }
}