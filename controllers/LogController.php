<?php

namespace app\controllers;

use Yii;
use app\models\Log;
use app\models\LogSearch;
use app\models\Blog;
use yii\web\Controller;
use yii\helpers\ArrayHelper;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

class LogController extends Controller
{

  public function behaviors()
  {
    return [
      'access' => [
        'class' => AccessControl::className(),
        'only' => ['index'],
        'rules' => [
          [
            'actions' => ['index'],
            'allow' => true,
            'roles' => ['@'],
          ],
        ],
      ],
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'index' => ['get'],
        ],
      ],
    ];
  }

  /**
   * Lists all Log models.
   * @return mixed
   */
  public function actionIndex()
  {
    $searchModel = new LogSearch(['userId' => Yii::$app->user->id]);
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
    $blogs = ArrayHelper::map(Blog::queryForIndex(Yii::$app->user->id)->all(), 'id', 'blogname');
    $actionTitles = Log::getActionTitles();

    return $this->render('index', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
      'blogs' => $blogs,
      'actionTitles' => $actionTitles,
    ]);
  }
}