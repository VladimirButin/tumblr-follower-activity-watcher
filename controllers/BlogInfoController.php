<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use app\models\Tumblr;
use app\models\BlogInfo;

class BlogInfoController extends Controller
{

  public function behaviors()
  {
    return [
      'access' => [
        'class' => AccessControl::className(),
        'only' => ['index'],
        'rules' => [
          [
            'actions' => ['index'],
            'allow' => true,
            'roles' => ['@'],
          ],
        ],
      ],
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'index' => ['get'],
        ],
      ],
    ];
  }

  public function actionIndex($blog)
  {
    $oauthClient = Yii::$app->oauth;
    $oauthClient->setAccessToken(Yii::$app->user->identity->getAccessTokenBlob());

    $tumblrData = Tumblr::getBlogInfo($blog);
    $blogInfo = new BlogInfo();
    $blogInfo->setAttributes($tumblrData);

    if (Yii::$app->getRequest()->isAjax) {
      return $this->renderPartial('index', ['blogInfo' => $blogInfo]);
    } else {
      return $this->render('index', ['blogInfo' => $blogInfo]);
    }
  }
}