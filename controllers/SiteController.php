<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use app\models\User;
use app\models\Blog;
use app\models\update\DataUpdate;
use app\models\Tumblr;

class SiteController extends Controller
{

  public function behaviors()
  {
    return [
      'access' => [
        'class' => AccessControl::className(),
        'only' => ['logout', 'update', 'access'],
        'rules' => [
          [
            'actions' => ['logout', 'update'],
            'allow' => true,
            'roles' => ['@'],
          ],
          [
            'actions' => ['access', 'login'],
            'allow' => true,
            'roles' => ['?'],
          ],
        ],
      ],
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'logout' => ['post'],
        ],
      ],
    ];
  }

  public function actions()
  {
    return [
      'error' => [
        'class' => 'yii\web\ErrorAction',
      ],
    ];
  }

  public function actionIndex()
  {
    if (Yii::$app->user->isGuest) {
      return $this->render('index');
    } else {
      $blogListQuery = Blog::queryForIndex(Yii::$app->user->id);
      $dataProvider = new ActiveDataProvider([
        'query' => $blogListQuery,
        'pagination' => false,
      ]);
      return $this->render('index', ['dataProvider' => $dataProvider]);
    }
  }

  public function actionLogin()
  {
    if (!\Yii::$app->user->isGuest) {
      return $this->goHome();
    }

    return $this->redirect(Tumblr::oauthStartLogin());
  }

  public function actionAccess()
  {
    if (!\Yii::$app->user->isGuest) {
      return $this->goHome();
    }

    $accessToken = Tumblr::oauthFinishLogin();

    $tumblrResponse = Tumblr::getUserInfo();

    $user = User::saveSession($tumblrResponse, $accessToken);
    Yii::$app->user->login($user);
    Blog::resetBlogs($user->id);
    Blog::setActiveBlogs($user->id, $tumblrResponse['blogs']);

    return $this->goHome();
  }

  public function actionLogout()
  {
    Yii::$app->user->logout();

    return $this->goHome();
  }

  public function actionUpdate()
  {
    $oauthClient = Yii::$app->oauth;
    $oauthClient->setAccessToken(Yii::$app->user->identity->getAccessTokenBlob());

    $dataUpdate = new DataUpdate();
    $dataUpdate->update(Yii::$app->user->id);

    Yii::$app->session->setFlash("alert", "Data has been updated!");
    return $this->goHome();
  }
}