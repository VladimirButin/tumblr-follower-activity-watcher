$(function () {
  var $modal = $('#blogInfoModal');
  var $modalContent = $modal.find('#modalContent');

  $(document).on('click', 'a.blog-info', function (e) {
    e.preventDefault();

    $.ajax({
      url: '/blog-info/index',
      method: 'get',
      data: {
        blog: $(this).data('blog'),
      }
    }).done(function (response) {
      $modalContent.html(response);
      $modal.modal('show');
    }).fail(function (_, status, error) {
      console.log(status);
      console.dir(error);
    });
  });
});