<?php

namespace app\events;

use yii\base\Event;
use yii\base\Exception;
use app\models\Followee;

class EventFolloweeAdded extends Event
{
  /**
   *
   * @var Followee
   */
  public $followee;

  public function __construct($config = array())
  {
    parent::__construct($config);
    if ($this->followee === null || !($this->followee instanceof Followee)) {
      throw new Exception("Event don't have correct followee");
    }
  }
}