<?php

namespace app\events;

use yii\base\Event;
use yii\base\Exception;
use app\models\Follower;

class EventFollowerLost extends Event
{
  /**
   *
   * @var Follower
   */
  public $follower;

  public function __construct($config = array())
  {
    parent::__construct($config);
    if ($this->follower === null || !($this->follower instanceof Follower)) {
      throw new Exception("Event don't have correct follower");
    }
  }
}