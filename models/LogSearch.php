<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Log;

/**
 * LogSearch represents the model behind the search form about `app\models\Log`.
 */
class LogSearch extends Log
{

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['id', 'userId', 'userBlog', 'action'], 'integer'],
      [['blogName', 'blogUrl', 'date', 'action', 'userBlog'], 'safe'],
    ];
  }

  /**
   * @inheritdoc
   */
  public function scenarios()
  {
    return [
      self::SCENARIO_DEFAULT => ['blogName', 'blogUrl', 'date', 'action', 'userBlog'],
    ];
  }

  /**
   * Creates data provider instance with search query applied
   *
   * @param array $params
   *
   * @return ActiveDataProvider
   */
  public function search($params)
  {
    $query = Log::find();
    $query->with('blog');

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'pagination' => [
        'pageSize' => Yii::$app->params['pagination']['logPageSize'],
      ],
      'sort' => [
        'defaultOrder' => [
          'date' => SORT_DESC,
          'id' => SORT_DESC,
        ]
      ]
    ]);

    $this->load($params);

    if (!$this->validate()) {
      $query->where('0=1');
      return $dataProvider;
    }

    $query->andFilterWhere([
      'id' => $this->id,
      'userId' => $this->userId,
      'userBlog' => $this->userBlog,
      'action' => $this->action,
    ]);

    if (!empty($this->date)) {
      $dates = explode(Yii::$app->params['dateSeparator'], $this->date);
      if (count($dates) == 2) {
        $query->andFilterWhere(['>=', 'date', Yii::$app->formatter->asDatetime($dates[0], 'php:U')]);

        $dates[1] = Yii::$app->formatter->asDatetime($dates[1], 'php:U');
        $dates[1] = strtotime('+1 day', $dates[1]);
        $query->andFilterWhere(['<', 'date', Yii::$app->formatter->asDatetime($dates[1], 'php:U')]);
      }
    }

    $query->andFilterWhere(['like', 'blogName', $this->blogName])
    ->andFilterWhere(['like', 'blogUrl', $this->blogUrl]);

    return $dataProvider;
  }
}