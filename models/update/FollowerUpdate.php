<?php

namespace app\models\update;

use Yii;
use yii\base\Object;
use yii\helpers\Json;
use app\events\EventFollowerAcquired;
use app\events\EventFollowerLost;
use app\models\Follower;

class FollowerUpdate extends Object
{

  const EVENT_FOLLOWER_ACQUIRED = 'follower_acquired';
  const EVENT_FOLLOWER_LOST = 'follower_lost';

  /**
   * Set user followers status to pending
   * @param int $userId
   * @param int $blogId
   */
  public static function setStatusToPending($userId, $blogId)
  {
    Follower::updateAll(['active' => Follower::STATUS_PENDING_ACTIVE], ['active' => Follower::STATUS_ACTIVE, 'userId' => $userId, 'blogId' => $blogId]);
    Follower::updateAll(['active' => Follower::STATUS_PENDING_INACTIVE], ['active' => Follower::STATUS_INACTIVE, 'userId' => $userId, 'blogId' => $blogId]);
  }

  /**
   * Process user info from tumblr api into record
   * @param int $userId
   * @param int $blogId
   * @param array $blogInfo
   */
  public static function processFollower($userId, $blogId, $blogInfo)
  {
    $add = false;

    $url = $blogInfo['url'];
    $follower = Follower::find()->where(['userId' => $userId, 'url' => $url, 'blogId' => $blogId])->one();
    if (empty($follower) || ($follower->active != Follower::STATUS_PENDING_ACTIVE && $follower->active != Follower::STATUS_ACTIVE)) {
      $add = true;
    }

    if (empty($follower)) {
      $follower = new Follower;
    }

    $follower->attributes = [
      'userId' => $userId,
      'blogId' => $blogId,
      'name' => $blogInfo['name'],
      'url' => $url,
      'active' => Follower::STATUS_ACTIVE,
      'info' => Json::encode($blogInfo),
    ];

    $follower->save();
    if ($add) {
      Yii::$app->trigger(self::EVENT_FOLLOWER_ACQUIRED, new EventFollowerAcquired(['follower' => $follower]));
    }
  }

  /**
   * Mark all remaining followers with pending status
   * @param int $userId
   * @param int $blogId
   */
  public static function processUnchecked($userId, $blogId)
  {
    Follower::updateAll(['active' => Follower::STATUS_INACTIVE], ['active' => Follower::STATUS_PENDING_INACTIVE, 'userId' => $userId, 'blogId' => $blogId]);
    $query = Follower::find()->where(['active' => Follower::STATUS_PENDING_ACTIVE, 'userId' => $userId, 'blogId' => $blogId]);

    foreach ($query->each() as $follower) {
      $follower->active = Follower::STATUS_INACTIVE;
      $follower->save();
      Yii::$app->trigger(self::EVENT_FOLLOWER_LOST, new EventFollowerLost(['follower' => $follower]));
    }
  }
}