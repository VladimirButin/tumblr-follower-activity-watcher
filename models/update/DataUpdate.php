<?php

namespace app\models\update;

use Yii;
use yii\base\Object;
use yii\base\Exception;
use app\models\Tumblr;
use app\models\Blog;

class DataUpdate extends Object
{

  public function init()
  {
    parent::init();

    Yii::$app->on(FolloweeUpdate::EVENT_FOLLOWEE_ADDED, ['app\models\update\LogHandler', 'followeeAdded']);
    Yii::$app->on(FolloweeUpdate::EVENT_FOLLOWEE_REMOVED, ['app\models\update\LogHandler', 'followeeRemoved']);
    Yii::$app->on(FollowerUpdate::EVENT_FOLLOWER_ACQUIRED, ['app\models\update\LogHandler', 'followerAcquired']);
    Yii::$app->on(FollowerUpdate::EVENT_FOLLOWER_LOST, ['app\models\update\LogHandler', 'followerLost']);    
  }

  public function update($userId)
  {
    $transaction = Yii::$app->getDb()->beginTransaction();
    try {
      FolloweeUpdate::setStatusToPending($userId);
      foreach (Tumblr::getFollowees() as $followee) {
        FolloweeUpdate::processFollowee($userId, $followee);
      }
      FolloweeUpdate::processUnchecked($userId);

      foreach (Blog::queryForIndex($userId)->each() as $blog) {
        FollowerUpdate::setStatusToPending($userId, $blog->id);
        foreach (Tumblr::getFollowers($blog->blogname) as $follower) {
          FollowerUpdate::processFollower($userId, $blog->id, $follower);
        }
        FollowerUpdate::processUnchecked($userId, $blog->id);
      }
      $transaction->commit();
    }
    catch (Exception $ex) {
        $transaction->rollBack();
        throw($ex);
    }
  }
}