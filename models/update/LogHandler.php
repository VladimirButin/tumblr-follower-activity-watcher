<?php

namespace app\models\update;

use yii\base\Object;
use app\events\EventFolloweeAdded;
use app\events\EventFolloweeRemoved;
use app\events\EventFollowerAcquired;
use app\events\EventFollowerLost;
use app\models\Log;
use app\models\Followee;
use app\models\Follower;

class LogHandler extends Object
{

  /**
   * Followee added handler
   * @param EventFolloweeAdded $event
   */
  static public function followeeAdded(EventFolloweeAdded $event)
  {
    static::followeeHandler($event->followee, Log::ACTION_ADDED_FOLLOWEE);
  }

  /**
   * Followee removed handler
   * @param EventFolloweeRemoved $event
   */
  static public function followeeRemoved(EventFolloweeRemoved $event)
  {
    static::followeeHandler($event->followee, Log::ACTION_REMOVED_FOLLOWEE);
  }

  /**
   * Saving followees changes
   * @param Followee $followee
   * @param int $action
   */
  static public function followeeHandler(Followee $followee, $action)
  {
    if (!($followee instanceof Followee)) {
      throw new Exception("Invalid class");
    }

    $logEntry = new Log();
    $logEntry->attributes = [
      'action' => $action,
      'userId' => $followee->userId,
      'blogName' => $followee->name,
      'blogUrl' => $followee->url
    ];
    $logEntry->save();
  }

  /**
   * Follower acquired handler
   * @param EventFollowerAcquired $event
   */
  static public function followerAcquired(EventFollowerAcquired $event)
  {
    static::followerHandler($event->follower, Log::ACTION_ACQUIRED_FOLLOWER);
  }

  /**
   * Follower lost handler
   * @param EventFollowerLost $event
   */
  static public function followerLost(EventFollowerLost $event)
  {
    static::followerHandler($event->follower, Log::ACTION_LOST_FOLLOWER);
  }

  /**
   * Saving followers changes
   * @param Follower $follower
   * @param int $action
   */
  static public function followerHandler(Follower $follower, $action)
  {
    if (!($follower instanceof Follower)) {
      throw new Exception("Invalid class");
    }

    $logEntry = new Log;
    $logEntry->attributes = [
      'action' => $action,
      'userId' => $follower->userId,
      'userBlog' => $follower->blogId,
      'blogName' => $follower->name,
      'blogUrl' => $follower->url
    ];
    $logEntry->save();
  }
}