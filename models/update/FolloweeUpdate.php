<?php

namespace app\models\update;

use Yii;
use yii\base\Object;
use yii\helpers\Json;
use app\events\EventFolloweeAdded;
use app\events\EventFolloweeRemoved;
use app\models\Followee;

class FolloweeUpdate extends Object
{

  const EVENT_FOLLOWEE_ADDED = 'followee_added';
  const EVENT_FOLLOWEE_REMOVED = 'followee_removed';

  /**
   * Set user followees status to pending
   * @param int $userId
   */
  public static function setStatusToPending($userId)
  {
    Followee::updateAll(['active' => Followee::STATUS_PENDING_ACTIVE], ['active' => Followee::STATUS_ACTIVE, 'userId' => $userId]);
    Followee::updateAll(['active' => Followee::STATUS_PENDING_INACTIVE], ['active' => Followee::STATUS_INACTIVE, 'userId' => $userId]);
  }

  /**
   * Process blog info from tumblr api into record
   * @param int $userId
   * @param array $blogInfo
   */
  public static function processFollowee($userId, $blogInfo)
  {
    $add = false;

    $url = $blogInfo['url'];
    $followee = Followee::find()->where(['userId' => $userId, 'url' => $url])->one();
    if (empty($followee) || ($followee->active != Followee::STATUS_PENDING_ACTIVE && $followee->active != Followee::STATUS_ACTIVE)) {
      $add = true;
    }

    if (empty($followee)) {
      $followee = new Followee();
    }

    $followee->attributes = [
      'userId' => $userId,
      'name' => $blogInfo['name'],
      'url' => $url,
      'active' => Followee::STATUS_ACTIVE,
      'info' => Json::encode($blogInfo),
    ];

    $followee->save();
    if ($add) {
      Yii::$app->trigger(self::EVENT_FOLLOWEE_ADDED, new EventFolloweeAdded(['followee' => $followee]));
    }
  }

  /**
   * Mark all remaining followees with pending status
   * @param int $userId
   */
  public static function processUnchecked($userId)
  {
    Followee::updateAll(['active' => Followee::STATUS_INACTIVE], ['active' => Followee::STATUS_PENDING_INACTIVE, 'userId' => $userId]);
    $query = Followee::find()->where(['active' => Followee::STATUS_PENDING_ACTIVE, 'userId' => $userId]);

    foreach ($query->each() as $followee) {
      $followee->active = Followee::STATUS_INACTIVE;
      $followee->save();
      Yii::$app->trigger(self::EVENT_FOLLOWEE_REMOVED, new EventFolloweeRemoved(['followee' => $followee]));
    }
  }
}