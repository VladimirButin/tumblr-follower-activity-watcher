<?php

namespace app\models;

use yii\helpers\Json;

/**
 * This is the model class for table "blog".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $blogname
 * @property integer $active
 * @property string $info
 */
class Blog extends \yii\db\ActiveRecord
{

  private $_convertedInfo = null;

  const SCENARIO_LOGIN = 'login';
  const SCENARIO_INDEX = 'index';
  const STATUS_INACTIVE = 0;
  const STATUS_ACTIVE = 1;

  /**
   * @inheritdoc
   */
  public static function tableName()
  {
    return 'blog';
  }

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['id'], 'integer'],
      [['userId'], 'required'],
      [['userId', 'active'], 'integer'],
      [['info'], 'string'],
      [['blogname'], 'string', 'max' => 100],
      [['userId', 'active', 'blogname'], 'required', 'on' => self::SCENARIO_LOGIN],
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'id' => 'ID',
      'userId' => 'User ID',
      'blogname' => 'Blog name',
      'active' => 'Active',
      'info' => 'Info',
    ];
  }

  /**
   * Set inactive status to all user blogs
   * @param int $userId
   */
  public static function resetBlogs($userId)
  {
    self::updateAll(['active' => self::STATUS_INACTIVE], ['userId' => $userId]);
  }

  /**
   * Updating blog data using tumblr response
   * @param type $userId
   * @param type $blogs
   */
  public static function setActiveBlogs($userId, $blogs)
  {
    foreach ($blogs as $info) {
      $blogname = $info['name'];
      $blog = static::find()->where(['userId' => $userId, 'blogname' => $blogname])->one();
      if (empty($blog)) {
        $blog = new static;
      }

      $blog->setScenario(self::SCENARIO_LOGIN);

      $blog->attributes = [
        'userId' => $userId,
        'blogname' => $blogname,
        'info' => Json::encode($info),
        'active' => self::STATUS_ACTIVE
      ];

      $blog->save();
    }
  }

  /**
   * Get query for data provider
   * @param int $userId
   * @return ActiveQuery
   */
  public static function queryForIndex($userId)
  {
    return static::find()->where(['userId' => $userId, 'active' => self::STATUS_ACTIVE]);
  }

  /**
   * Converting blog info from JSON to array
   * @return Array
   */
  public function getConvertedInfo()
  {
    if (!empty($this->info) && empty($this->_convertedInfo)) {
      $this->_convertedInfo = Json::decode($this->info);
    }
    return $this->_convertedInfo;
  }

  /**
   * Getting blog data
   * @param int $userId
   * @param int $blogId
   * @return null|ActiveRecord
   */
  public static function getByUserAndId($userId, $blogId)
  {
    return static::find()->where(['id' => $blogId, 'userId' => $userId, 'active' => self::STATUS_ACTIVE])->one();
  }
}