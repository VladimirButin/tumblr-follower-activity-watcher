<?php

namespace app\models;

use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\authclient\OAuthToken;
use yii\helpers\Json;
use yii\behaviors\TimestampBehavior;

class User extends ActiveRecord implements IdentityInterface
{

  const SCENARIO_LOGIN = 'login';

  /**
   * @inheritdoc
   */
  public static function tableName()
  {
    return 'user';
  }

  /**
   * @inheritdoc
   */
  public function behaviors()
  {
    return array_merge(parent::behaviors(), [
      [
        'class' => TimestampBehavior::className(),
        'createdAtAttribute' => 'lastLogin',
        'updatedAtAttribute' => 'lastLogin',
        'value' => function() {
          return date('U');
        },
      ],
    ]);
  }

  public function scenarios()
  {
    return [
      self::SCENARIO_LOGIN => ['username', 'info', 'access_token_blob'],
    ];
  }

  public function rules()
  {
    return [
      [['username', 'access_token_blob'], 'required', 'on' => self::SCENARIO_LOGIN],
    ];
  }

  /**
   * @inheritdoc
   */
  public static function findIdentity($id)
  {
    $user = static::find()->where(['id' => $id])->one();
    return $user ? $user : null;
  }

  /**
   * @inheritdoc
   */
  public static function findIdentityByAccessToken($token, $type = null)
  {
    return null;
  }

  /**
   * @inheritdoc
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   *      
   * @inheritdoc
   */
  public function beforeSave($insert)
  {
    if (parent::beforeSave($insert)) {
      $this->authKey = \Yii::$app->security->generateRandomString();
      return true;
    }
    return false;
  }

  /**
   * @inheritdoc
   */
  public function getAuthKey()
  {
    return $this->authKey;
  }

  /**
   * @inheritdoc
   */
  public function validateAuthKey($authKey)
  {
    return $this->authKey === $authKey;
  }

  /**
   * Save session data to table
   * 
   * @param array $tumblrResponse
   * @param OAuthToken $accessToken
   */
  public static function saveSession($tumblrResponse, OAuthToken $accessToken)
  {
    $username = $tumblrResponse['name'];

    $user = static::find()->where(['username' => $username])->one();
    if (empty($user)) {
      $user = new static;
    }

    $user->setScenario(self::SCENARIO_LOGIN);

    $user->attributes = [
      'username' => $username,
      'info' => Json::encode($tumblrResponse),
      'access_token_blob' => serialize($accessToken),
    ];

    $user->save();
    return $user;
  }

  /**
   * Getting user access token
   * @return OAuthToken
   */
  public function getAccessTokenBlob()
  {
    return unserialize($this->access_token_blob);
  }
}