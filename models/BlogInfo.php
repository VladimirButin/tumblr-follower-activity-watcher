<?php

namespace app\models;

use yii\base\Model;

class BlogInfo extends Model
{

  public $title;
  public $name;
  public $description;
  public $posts;
  public $likes;
  public $url;
  public $updated;
  public $is_nsfw;
  public $share_likes;

  public function rules()
  {
    return array_merge(parent::rules(), [
      [['title', 'name', 'description'], 'string'],
      [['posts', 'likes', 'updated'], 'integer'],
      ['url', 'url'],
      [['is_nsfw', 'share_likes'], 'boolean'],
      [['title', 'name', 'description', 'posts', 'likes', 'updated', 'url', 'is_nsfw', 'share_likes'], 'safe'],
    ]);
  }

  public function attributeLabels()
  {
    return array_merge(parent::attributeLabels(), [
      'title' => 'Title',
      'name' => 'Blog name',
      'url' => 'Blog URL',
      'description' => 'Description',
      'posts' => 'Posts',
      'likes' => 'Likes',
      'updated' => 'Updated',
      'is_nsfw' => 'Is NSFW',
      'share_likes' => 'Likes are shared',
    ]);
  }
}