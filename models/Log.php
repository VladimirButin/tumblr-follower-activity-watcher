<?php

namespace app\models;

use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "log".
 *
 * @property integer $id
 * @property integer $userId
 * @property string $userBlog
 * @property string $blogName
 * @property string $blogUrl
 * @property integer $action
 * @property string $date
 */
class Log extends \yii\db\ActiveRecord
{

  const ACTION_ADDED_FOLLOWEE = 1;
  const ACTION_REMOVED_FOLLOWEE = 2;
  const ACTION_ACQUIRED_FOLLOWER = 3;
  const ACTION_LOST_FOLLOWER = 4;

  /**
   * @inheritdoc
   */
  public static function tableName()
  {
    return 'log';
  }

  /**
   * @inheritdoc
   */
  public function behaviors()
  {
    return array_merge(parent::behaviors(), [
      [
        'class' => TimestampBehavior::className(),
        'createdAtAttribute' => 'date',
        'updatedAtAttribute' => null,
        'value' => function() {
          return date('U');
        },
      ],
    ]);
  }

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['userId', 'blogName', 'blogUrl', 'action'], 'required'],
      [['userBlog', 'userId', 'action'], 'integer'],
      [['blogName', 'blogUrl'], 'string', 'max' => 100],
      ['date', 'string'],
      ['userBlog', 'required', 'when' => function($model) {
        return in_array($model->action, [self::ACTION_ACQUIRED_FOLLOWER, self::ACTION_LOST_FOLLOWER]);
      }],
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'id' => 'ID',
      'userId' => 'User ID',
      'userBlog' => 'User Blog',
      'blogName' => 'Blog Name',
      'blogUrl' => 'Blog Url',
      'action' => 'Action',
      'date' => 'Date',
    ];
  }

  public static function getActionTitles()
  {
    return [
      Log::ACTION_ADDED_FOLLOWEE => 'Added followee',
      Log::ACTION_REMOVED_FOLLOWEE => 'Removed followee',
      Log::ACTION_ACQUIRED_FOLLOWER => 'Acquired follower',
      Log::ACTION_LOST_FOLLOWER => 'Lost follower',
    ];
  }

  public function getBlog()
  {
    return $this->hasOne(Blog::className(), ['id' => 'userBlog', 'userId' => 'userId']);
  }
}