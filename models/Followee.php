<?php

namespace app\models;

use yii\helpers\Json;

/**
 * This is the model class for table "followee".
 *
 * @property integer $id
 * @property integer $userId
 * @property string $name
 * @property integer $active
 * @property string $info
 */
class Followee extends \yii\db\ActiveRecord
{

  private $_convertedInfo = null;

  const STATUS_INACTIVE = 0;
  const STATUS_ACTIVE = 1;
  const STATUS_PENDING_ACTIVE = 2;
  const STATUS_PENDING_INACTIVE = 3;

  /**
   * @inheritdoc
   */
  public static function tableName()
  {
    return 'followee';
  }

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['userId', 'url'], 'required'],
      [['userId', 'active'], 'integer'],
      [['info'], 'string'],
      [['name'], 'string', 'max' => 100],
      [['url'], 'string', 'max' => 100]
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'id' => 'ID',
      'userId' => 'User ID',
      'name' => 'Name',
      'url' => 'URL',
      'active' => 'Active',
      'info' => 'Info',
    ];
  }

  /**
   * Converting blog info from JSON to array
   * @return Array
   */
  public function getConvertedInfo()
  {
    if (!empty($this->info) && empty($this->_convertedInfo)) {
      $this->_convertedInfo = Json::decode($this->info);
    }
    return $this->_convertedInfo;
  }

  /**
   * Query for data provider with all active followees
   * @param int $userId
   * @return yii\db\ActiveQuery
   */
  public static function getActiveFolloweesQuery($userId)
  {
    return Followee::find()->where(['userId' => $userId, 'active' => Followee::STATUS_ACTIVE])->orderBy(['name' => 'asc']);
  }
}