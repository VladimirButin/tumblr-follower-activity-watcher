<?php

namespace app\models;

use yii\helpers\Json;

/**
 * This is the model class for table "follower".
 *
 * @property integer $id
 * @property integer $userId
 * @property integer $blogId
 * @property string $name
 * @property string $url
 * @property integer $active
 * @property string $info
 */
class Follower extends \yii\db\ActiveRecord
{

  private $_convertedInfo = null;

  const STATUS_INACTIVE = 0;
  const STATUS_ACTIVE = 1;
  const STATUS_PENDING_ACTIVE = 2;
  const STATUS_PENDING_INACTIVE = 3;

  /**
   * @inheritdoc
   */
  public static function tableName()
  {
    return 'follower';
  }

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['userId', 'blogId', 'name', 'url'], 'required'],
      [['userId', 'blogId', 'active'], 'integer'],
      [['info'], 'string'],
      [['name', 'url'], 'string', 'max' => 100]
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'id' => 'ID',
      'userId' => 'User ID',
      'blogId' => 'Blog ID',
      'name' => 'Name',
      'url' => 'URL',
      'active' => 'Active',
      'info' => 'Info',
    ];
  }

  /**
   * Converting blog info from JSON to array
   * @return Array
   */
  public function getConvertedInfo()
  {
    if (!empty($this->info) && empty($this->_convertedInfo)) {
      $this->_convertedInfo = Json::decode($this->info);
    }
    return $this->_convertedInfo;
  }

  /**
   * Query for data provider with all active followers
   * @param int $userId
   * @param int $blogId
   * @return yii\db\ActiveQuery
   */
  public static function getActiveFollowersQuery($userId, $blogId)
  {
    return Follower::find()->where(['userId' => $userId, 'active' => self::STATUS_ACTIVE, 'blogId' => $blogId])->orderBy(['name' => 'asc']);
  }
}