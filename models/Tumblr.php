<?php

namespace app\models;

use Yii;
use yii\base\Object;
use yii\helpers\Url;
use yii\base\Exception;

class Tumblr extends Object
{

  /**
   * Start login process
   * Fetches request token and builds url to login via Tumblr
   * @return string
   */
  public static function oauthStartLogin()
  {
    $oauthClient = Yii::$app->oauth;
    $oauthClient->returnUrl = Url::to(['/site/access'], true);
    $requestToken = $oauthClient->fetchRequestToken();
    Yii::$app->session->set('requestToken', $requestToken);

    return $oauthClient->buildAuthUrl($requestToken);
  }

  /**
   * Finish login process
   * Save user info to session
   * Returns access token
   * @return string
   */
  public static function oauthFinishLogin()
  {
    $oauthClient = Yii::$app->oauth;
    $requestToken = Yii::$app->session->get('requestToken');
    $oauthClient->setAccessToken($requestToken);

    $accessToken = $oauthClient->fetchAccessToken($requestToken);

    return $accessToken;
  }

  /**
   * Return user info from tumblr
   * @return array
   * @throws Exception
   */
  public static function getUserInfo()
  {
    $tumblrParams = Yii::$app->params['tumblr'];
    $response = Yii::$app->oauth->api($tumblrParams['userInfo'], 'GET');

    if (empty($response)) {
      throw new Exception("User info api response is empty");
    }
    if ($response['meta']['status'] != $tumblrParams['successStatus']) {
      throw new Exception("User info api error: " . $response['meta']['msg']);
    }

    return $response['response']['user'];
  }

  /**
   * Generator, returning all followeers for user
   * @throws Exception
   */
  public static function getFollowees()
  {
    $url = Yii::$app->params['tumblr']['followees'];
    $limit = Yii::$app->params['tumblr']['limit'];

    for (
    $offset = 0; $response = Yii::$app->oauth->api($url, 'GET', ['offset' => $offset, 'limit' => $limit]); $offset += $limit
    ) {
      if (empty($response)) {
        throw new Exception("Tumblr api error: no response");
      }
      if ($response['meta']['status'] != Yii::$app->params['tumblr']['successStatus']) {
        throw new Exception("Tumblr api error: " . $response['meta']['msg']);
      }

      if (count($response['response']['blogs']) == 0) {
        break;
      }

      for ($index = 0; $index < count($response['response']['blogs']); $index++) {
        yield $response['response']['blogs'][$index];
      }
    }
  }

  /**
   * Generator, return all followers for users blog
   * @param type $blogName
   * @throws Exception
   */
  public static function getFollowers($blogName)
  {
    $url = sprintf(Yii::$app->params['tumblr']['followers'], $blogName . ".tumblr.com");
    $limit = Yii::$app->params['tumblr']['limit'];

    for (
    $offset = 0; $response = Yii::$app->oauth->api($url, 'GET', ['offset' => $offset, 'limit' => $limit]); $offset+=$limit
    ) {
      if (empty($response)) {
        throw new Exception("Tumblr api error: no response");
      }
      if ($response['meta']['status'] != Yii::$app->params['tumblr']['successStatus']) {
        throw new Exception("Tumblr api error: " . $response['meta']['msg']);
      }

      if (count($response['response']['users']) == 0) {
        break;
      }

      for ($index = 0; $index < count($response['response']['users']); $index++) {
        yield $response['response']['users'][$index];
      }
    }
  }

  public static function getBlogInfo($blogName)
  {
    $url = sprintf(Yii::$app->params['tumblr']['blogInfo'], $blogName . ".tumblr.com");
    $response = Yii::$app->oauth->api($url, 'GET', ['api_key' => Yii::$app->oauth->consumerKey]);

    if (empty($response)) {
      throw new Exception("Tumblr api error: no response");
    }
    if ($response['meta']['status'] != Yii::$app->params['tumblr']['successStatus']) {
      throw new Exception("Tumblr api error: " . $response['meta']['msg']);
    }

    return $response['response']['blog'];
  }
}