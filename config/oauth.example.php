<?php
return [
  'class' => 'yii\authclient\OAuth1',
  'version' => '1.0a',
  'requestTokenMethod' => 'POST',
  'requestTokenUrl' => 'https://www.tumblr.com/oauth/request_token',
  'consumerKey' => 'Consumer Key',
  'consumerSecret' => 'Consumer Secret',
  'authUrl' => 'https://www.tumblr.com/oauth/authorize',
  'accessTokenMethod' => 'POST',
  'accessTokenUrl' => 'https://www.tumblr.com/oauth/access_token',
  'name' => 'tumblr',
  'title' => 'Tumblr',
  'apiBaseUrl' => 'https://api.tumblr.com/v2',
];
