<?php
return [
  'adminEmail' => 'admin@example.com',
  'tumblr' => [
    'userInfo' => 'user/info',
    'followees' => 'https://api.tumblr.com/v2/user/following',
    'followers' => 'https://api.tumblr.com/v2/blog/%s/followers',
    'blogInfo' => 'https://api.tumblr.com/v2/blog/%s/info',
    'successStatus' => '200',
    'limit' => 20,
  ],
  'pagination' => [
    'pageSize' => 16,
    'logPageSize' => 20,
  ],
  'dateSeparator' => ' / ',
];
