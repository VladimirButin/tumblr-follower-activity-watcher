<?php
return [
  'class' => 'yii\db\Connection',
  'dsn' => 'mysql:host=localhost;dbname=tfaw',
  'username' => 'DB user',
  'password' => 'DB password',
  'charset' => 'utf8',
];
