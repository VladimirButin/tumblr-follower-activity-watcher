<?php
Yii::setAlias('@tests', dirname(__DIR__) . '/tests');

$params = require(__DIR__ . '/params.php');
$db = require(__DIR__ . '/db.php');

return [
  'id' => 'tfaw-console',
  'basePath' => dirname(__DIR__),
  'bootstrap' => ['log', 'gii'],
  'controllerNamespace' => 'app\commands',
  'modules' => [
    'gii' => 'yii\gii\Module',
  ],
  'components' => [
    'cache' => [
      'class' => 'yii\caching\FileCache',
    ],
    'log' => [
      'targets' => [
        [
          'class' => 'yii\log\FileTarget',
          'levels' => ['error', 'warning'],
        ],
      ],
    ],
    'db' => require(__DIR__ . '/db.php'),
    'oauth' => require(__DIR__ . '/oauth.php'),
  ],
  'params' => $params,
];
